<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $table = 'building';

    protected $fillable = [
        'title',
        'street',
    ];

    public function sensors()
    {
        return $this->hasMany(Sensor::class);
    }
}
