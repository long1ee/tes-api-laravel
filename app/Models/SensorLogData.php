<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SensorLogData extends Model
{
    protected $table = 'sensor_log_data';

    protected $fillable = [
        'sensor_id',
        'temp_value',
    ];
}
