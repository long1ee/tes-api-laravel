<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    protected $table = 'sensor';

    protected $fillable = [
        'building_id',
        'title',
    ];

    public function logData()
    {
        return $this->hasMany(SensorLogData::class);
    }
}
