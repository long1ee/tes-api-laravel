<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BuildingCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = [];
        $result['currentPage'] = $this->resource->currentPage();
        $result['perPage'] = $this->resource->perPage();
        $result['totalItems'] = $this->resource->total();
        foreach ($this->resource as $item) {
            $result['items'] = [
                'id' => $item->id,
                'title' => $item->title,
                'street' => $item->street,
                'sensor_ids' => array_map(function ($sensor) {
                    return $sensor['id'];
                }, $item->sensors->toArray()),
                'created_at' => date('Y-m-d H:i:s', strtotime($item->created_at)),
                'updated_at' => date('Y-m-d H:i:s', strtotime($item->updated_at)),
            ];
        }
        return $result;
    }
}
