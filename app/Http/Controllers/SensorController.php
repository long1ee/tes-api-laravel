<?php

namespace App\Http\Controllers;

use App\Http\Resources\SensorCollection;
use App\Http\Resources\SensorResource;
use App\Models\Sensor;
use App\Models\SensorLogData;
use App\Services\SensorService;
use Illuminate\Http\Request;

class SensorController extends Controller
{

    /**
     * @return SensorCollection
     */
    public function index(Request $request)
    {
        $pageId = ($request->get('page') ? (int) $request->get('page') : 10);
        $data = Sensor::paginate($pageId);
        return new SensorCollection($data);
    }

    public function show(Sensor $sensor)
    {
        return new SensorResource($sensor);
    }

    public function getAwgTemperature($sensorId, Request $request)
    {
        $time = ($request->request->get('time') ? (int) $request->request->get('time') : time());
        $hours = ($request->request->get('hours') ? (int) $request->request->get('hours') : 1);
        $result = (new SensorService())->getAwgTemperatureByTime($sensorId, $time, $hours);
        return $result;
    }



    //TODO не прописывал эти методы
    public function store()
    {
        //
    }

    public function update()
    {
        //
    }

    public function destroy()
    {
        //
    }
}
