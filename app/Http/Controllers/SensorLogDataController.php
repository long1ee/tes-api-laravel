<?php

namespace App\Http\Controllers;

use App\Models\SensorLogData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class SensorLogDataController extends Controller
{


    public function setNewLog($sensorId, Request $request)
    {
        $request->request->add(['sensor_id' => $sensorId]);
        $rules = [
            'sensor_id' => 'required|integer',
            'temp_value' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ];
        $msg = [
            'sensor_id' => 'set sensor_id val',
            'temp_value' => 'set temp_value val',
        ];
        $validator = Validator::make($request->request->all(), $rules, $msg);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }
        $logModel = new SensorLogData($request->request->all());
        if ($logModel->save()) {
            return [
                'id' => $logModel->id,
            ];
        }
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}