<?php

namespace App\Http\Controllers;

use App\Models\Building;
use Illuminate\Http\Request;
use App\Http\Resources\BuildingResource;
use App\Http\Resources\BuildingCollection;
use Illuminate\Http\Response;

class BuildingController extends Controller
{

    /**
     * @return BuildingCollection
     */
    public function index(Request $request)
    {
        $pageId = ($request->get('page') ? (int) $request->get('page') : 10);
        return new BuildingCollection(Building::paginate($pageId));
    }


    /**
     * @param Building $building
     * @return BuildingResource
     */
    public function show(Building $building)
    {
        return new BuildingResource($building);
    }




    //TODO не прописывал эти методы
    public function store()
    {
        //
    }


    public function update()
    {
        //
    }

    public function destroy()
    {
        //
    }
}
