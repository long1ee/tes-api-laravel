<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 04.12.19
 * Time: 17:21
 */

namespace App\Services;


use App\Models\Sensor;
use App\Models\SensorLogData;

class SensorService
{

    public function getAwgTemperatureByTime($sensorId, $time, $hours)
    {
        $result = [
            'sensor_id' => $sensorId,
            'message' => 'OK',
            'awg_temperature' => '',
        ];
        $minTime = date('Y-m-d H:i:s', ($time - (3600*$hours)));
        $maxTime = date('Y-m-d H:i:s', $time);
        $sensorLogData = SensorLogData::where('sensor_id', $sensorId)
            ->where('created_at', '>=', $minTime)
            ->where('created_at', '<=', $maxTime)
            ->get();
        $tempArr = $sensorLogData->pluck('temp_value')->toArray();

        if (!empty($tempArr)) {
            $result['awg_temperature'] = round(array_sum($tempArr)/count($tempArr), 2);
        } else {
            $result['message'] = 'No log data';
        }
        return $result;
    }
}