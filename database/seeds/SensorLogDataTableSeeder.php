<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SensorLogDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $minTime = strtotime(date('Y-m-d 00:00:00'));
        $maxTime = strtotime(date('Y-m-d 23:59:59'));
        for ($time = $minTime; $time<=$maxTime; $time+=300) {
            for ($sensorId = 1; $sensorId <=10; $sensorId++) {
                $data[] = [
                    'sensor_id' => $sensorId,
                    'temp_value' => mt_rand(15*10,85*10)/10,
                    'created_at' => date('Y-m-d H:i:s', $time),
                ];
            }
        }

        DB::table('sensor_log_data')->insert($data);
    }
}
