<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BuildingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        for ($i = 1; $i<=10; $i++) {
            $data[] = [
                'title' => 'Здание ' . $i,
                'street' => 'Улица ' . $i,
            ];
        }

        DB::table('building')->insert($data);
    }
}
