<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SensorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        for ($i = 1; $i<=50; $i++) {
            $data[] = [
                'title' => 'Сенсор ' . $i,
                'building_id' => rand(1,10),
            ];
        }

        DB::table('sensor')->insert($data);
    }
}
