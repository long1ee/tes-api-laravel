<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateSensorLogDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sensor_log_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sensor_id')->unsigned();
            $table->decimal('temp_value', 5, 2);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->foreign('sensor_id', 'sensor_log_data_sensor_id_foreign')->references('id')->on('sensor')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sensor_log_data', function(Blueprint $table)
        {
            $table->dropForeign('sensor_log_data_sensor_id_foreign');
        });
        Schema::dropIfExists('sensor_log_data');
    }
}
