<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::fallback(function(){
    return response()->json(['message' => 'Operation Not Found!'], 404);
});

/* Buildings resource*/
Route::apiResource('/building','BuildingController');

/* Sensors resource*/
Route::apiResource('/sensor','SensorController');

Route::prefix('/sensor')->group(function () {
    /* Set new temperature data by sensor*/
    Route::get('{sensor_id}/set-temperature','SensorLogDataController@setNewLog');
    /* Get awg temperature value*/
    Route::get('{sensor_id}/awg-temperature','SensorController@getAwgTemperature');
});
