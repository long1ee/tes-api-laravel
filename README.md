GENERAL ROUTES

Buildings
- Get All [GET] http://localhost:8000/buildings
- Get building by Id [GET] http://localhost:8000/buildings/
    - param {ID} - building id
    
Sensors
- Get All [GET] http://localhost:8000/sensors
- Get sensor by Id [GET] http://localhost:8000/sensors/{ID}
- param {ID} - sensor id
- Set sensor temperature [POST] http://localhost:8000/sensors/{ID}/set-temperature
    - params : temp_value - decimal

- Get sensor AWG Temperature value [GET] http://localhost:8000/sensors/{ID}/awg-temperature?time={TIME}&hours={HOURS}
    - param {TIME} - timestamp, default current timestamp
    - param {HOURS} - count of hours, default 1
